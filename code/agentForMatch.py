from minimax import Minimax
from tree import Tree
from random import randint
import copy
import time
import random

class AgentForMatch:
	def __init__(self, color, oppopnentColor, time=None):
		self.color = color
		self.oppopnentColor = oppopnentColor
		self.from_cell = (0,0)
		self.to_cell = (0,0)
		self.height = 3
		self.myMove = -1
		self.wpoints = [[1000, 1000, 1000, 1000, 1000, 1000],[8, 30, 800, 800, 30, 8],[9, 20, 20, 20, 20, 9],[10, 10, 10, 10, 10, 10],[900, 900, 900, 900, 900, 900],[1000, 1000, 1000, 1000, 1000, 1000]]
		self.bpoints = [[1000, 1000, 1000, 1000, 1000, 1000],[900, 900, 900, 900, 900, 900],[10, 10, 10, 10, 10, 10],[9, 20, 20, 20, 20, 9],[8, 30, 800, 800, 30, 8],[1000, 1000, 1000, 1000, 1000, 1000]]

	def forcedMove(self, node):
		if self.color == 'B':
			for x in range(0,6):
				if node[1][x] == 'B' and (x+1 < 6 and node[0][x+1] == 'W'):
					self.to_cell = (0, x+1)
					self.from_cell = (1, x)
					return True
				elif node[1][x] == 'B' and (x-1 > -1 and node[0][x-1] == 'W'):
					self.to_cell = (0, x-1)
					self.from_cell = (1, x)
					return True
				elif node[1][x] == 'B' and node[0][x] == 'E':
					self.to_cell = (0, x)
					self.from_cell = (1, x)
					return True
				if node[4][x] == 'W' and (x+1 < 6 and node[5][x+1] == 'B'):
					self.from_cell = (5, x+1)
					self.to_cell = (4, x)
					return True
				elif node[4][x] == 'W' and (x-1 > -1 and node[5][x-1] == 'B'):
					self.from_cell = (5, x-1)
					self.to_cell = (4, x)
					return True
		else:
			for x in range(0,6):
				if node[4][x] == 'W' and (x+1 < 6 and node[5][x+1] == 'B'):
					self.to_cell = (5, x+1)
					self.from_cell = (4, x)
					return True
				elif node[4][x] == 'W' and (x-1 > -1 and node[5][x-1] == 'B'):
					self.to_cell = (5, x-1)
					self.from_cell = (4, x)
					return True
				elif node[4][x] == 'W' and node[5][x] == 'E':
					self.to_cell = (5, x)
					self.from_cell = (4, x)
					return True
				if node[1][x] == 'B' and (x+1 < 6 and node[0][x+1] == 'W'):
					self.from_cell = (0, x+1)
					self.to_cell = (1, x)
					return True
				elif node[1][x] == 'B' and (x-1 > -1 and node[0][x-1] == 'W'):
					self.from_cell = (0, x-1)
					self.to_cell = (1, x)
					return True
			

	def move(self, board):
		# default_timer = time.time
		# start = default_timer()
		if self.forcedMove(board.board):
			return self.from_cell, self.to_cell
		self.nextMove(board.board)
		from_cell, to_cell = self.nextMovement(board.board, self.color, self.oppopnentColor)
		# duration = (default_timer() - start)
		# print(duration)
		# print(from_cell)
		# print(to_cell)
		return from_cell, to_cell

	def nextMove(self, nodeBoard):
		alfa = -float('inf')
		beta = float('inf')
		self.alphabeta(nodeBoard, self.height, alfa, beta, True)

	def isTerminal(self, children, node):
		if len(children) == 0:
			return True
		return False

	def alphabeta(self, node, depth, alfa, beta, maximizingPlayer):
		children = []
		i = 0
		self.myMove = 0
		if depth == 0:
			return self.utility(node, maximizingPlayer)
		if maximizingPlayer:
			children = self.generateChildren(node, self.color, self.oppopnentColor)
		else:
			children = self.generateChildren(node, self.oppopnentColor, self.color)
		if self.isTerminal(children, node):
			return self.utility(node, maximizingPlayer)
		if maximizingPlayer:
			value = -float('inf')
			for child in children:
				pv = value
				value = max(value, self.alphabeta(child, depth - 1, alfa, beta, False))
				if not pv == value:
					self.myMove = i
				alfa = max(alfa, value)
				if alfa >= beta:
					break
				i += 1
			return value
		else:
			value = float('inf')
			for child in children:
				value = min(value, self.alphabeta(child, depth - 1, alfa, beta, True))
				beta = min(beta, value)
				if alfa >= beta:
					break
			return value

	def generateChildren(self, nodeBoard, color, opcolor):
		res = []
		for x in range(0, 6):
			for y in range(0, 6):
				if nodeBoard[x][y] == color and color == 'B':
					if x-1 >= 0 and y-1 >= 0 and (nodeBoard[x-1][y-1] == 'E' or nodeBoard[x-1][y-1] == opcolor):
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x-1][y-1] = color
						res.append(temp)
					if x-1 >= 0 and nodeBoard[x-1][y] == 'E':
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x-1][y] = color
						res.append(temp)
					if x-1 >= 0 and y+1 <= 5 and (nodeBoard[x-1][y+1] == 'E' or nodeBoard[x-1][y+1] == opcolor):
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x-1][y+1] = color
						res.append(temp)
				elif nodeBoard[x][y] == color and color == 'W':
					if x+1 < 6 and y-1 >= 0 and (nodeBoard[x+1][y-1] == 'E' or nodeBoard[x+1][y-1] == opcolor):
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x+1][y-1] = color
						res.append(temp)
					if x+1 < 6 and nodeBoard[x+1][y] == 'E':
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x+1][y] = color
						res.append(temp)
					if x+1 < 6 and y+1 <= 5 and (nodeBoard[x+1][y+1] == 'E' or nodeBoard[x+1][y+1] == opcolor):
						temp = copy.deepcopy(nodeBoard)
						temp[x][y] = 'E'
						temp[x+1][y+1] = color
						res.append(temp)

		return res

	def printGrid(self, list):
		for x in list:
			for y in x:
				for k in y:
					print(k, end=' ')
				print()
			print()

	def nextMovement(self, nodeBoard, color, opcolor):
		i = -1
		from_cell = (0,0)
		to_cell = (0,0)
		for x in range(0, 6):
			if i == self.myMove:
				return from_cell, to_cell
			for y in range(0, 6):
				if i == self.myMove:
					return from_cell, to_cell
				if nodeBoard[x][y] == color and color == 'B':
					if x-1 >= 0 and y-1 >= 0 and (nodeBoard[x-1][y-1] == 'E' or nodeBoard[x-1][y-1] == opcolor):
						from_cell = (x,y)
						to_cell = (x-1,y-1)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell
					if x-1 >= 0 and nodeBoard[x-1][y] == 'E':
						from_cell = (x,y)
						to_cell = (x-1,y)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell
					if x-1 >= 0 and y+1 <= 5 and (nodeBoard[x-1][y+1] == 'E' or nodeBoard[x-1][y+1] == opcolor):
						from_cell = (x,y)
						to_cell = (x-1,y+1)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell
				elif nodeBoard[x][y] == color and color == 'W':
					if x+1 < 6 and y-1 >= 0 and (nodeBoard[x+1][y-1] == 'E' or nodeBoard[x+1][y-1] == opcolor):
						from_cell = (x,y)
						to_cell = (x+1,y-1)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell
					if x+1 < 6 and nodeBoard[x+1][y] == 'E':
						from_cell = (x,y)
						to_cell = (x+1,y)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell
					if x+1 < 6 and y+1 <= 5 and (nodeBoard[x+1][y+1] == 'E' or nodeBoard[x+1][y+1] == opcolor):
						from_cell = (x,y)
						to_cell = (x+1,y+1)
						i += 1
					if i == self.myMove:
						return from_cell, to_cell

		return from_cell, to_cell

	def perc(self, node, x, y, color):
		count = 0
		patern = [[1,0],[1,1],[1,-1],[2,0],[2,1],[2,-1]]
		if color == 'B':
			for i in patern:
				if x-i[0] > -1 and y+i[1] < 6 and (node[x-i[0]][y+i[1]] == 'B' or node[x-i[0]][y+i[1]] == 'E'):
					if node[x-i[0]][y+i[1]] == 'E':
						count+=2
					else:
						count += 1
			return count/12
		else:
			for i in patern:
				if x+i[0] < 6 and y+i[1] < 6 and (node[x+i[0]][y+i[1]] == 'W' or node[x+i[0]][y+i[1]] == 'E'):
					if node[x+i[0]][y+i[1]] == 'E':
						count+=2
					else:
						count += 1
			return count/12

	
	def utility(self, node, black):
		WinValue = 500000
		PieceAlmostWinValue = 5000000
		wnum = 0
		bnum = 0
		bpoints = 0
		wpoints = 0
		bwin = False
		wwin = False
		Value = 0
		for x in range(0,6):
			for y in range(0,6):
				if node[x][y] == 'B':
					bnum += 1
					if x == 0:
						bwin = True
					bpoints += self.bpoints[x][y]
					if not ((x-1 > -1 and y+1 < 6 and node[x-1][y+1] == 'W')\
					or (x-1 > -1 and y-1 > -1 and node[x-1][y-1] == 'W')):
						bpoints += (float(self.bpoints[x][y])/2)
					bpoints += self.bpoints[x][y]*self.perc(node, x, y, node[x][y])*(0.5)
				else:
					wnum +=1
					if x == 5:
						wwin = True
					wpoints += self.wpoints[x][y]
					if not ((x+1 < 6 and y+1 < 6 and node[x+1][y+1] == 'B')\
					or (x+1 < 6 and y-1 > -1 and node[x+1][y-1] == 'B')):
						wpoints += (float(self.wpoints[x][y])/2)
					wpoints += self.wpoints[x][y]*self.perc(node, x, y, node[x][y])*(0.6)
		if (wnum == 0):
			bwin = True
		if (bnum == 0):
			wwin = True
		if wwin:
			wpoints += WinValue
		if bwin:
			bpoints += WinValue
		if black and self.color == 'B':
			return (bnum-(2*wnum))*20 + (bpoints-(2*wpoints))
		elif black and self.color == 'W':
			return (((wnum-(2*bnum))*20 + (wpoints-(2*bpoints))))
		elif not black and self.color == 'B':
			return -1*(((wnum-(2*bnum))*20 + (wpoints-(2*bpoints))))
		elif not black and self.color == 'W':
			return -1*((bnum-(2*wnum))*20 + (bpoints-(2*wpoints)))